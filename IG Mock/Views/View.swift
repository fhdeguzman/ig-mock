//
//  View.swift
//  IG Mock
//
//  Created by Franz Henri De Guzman on 10/26/20.
//  Copyright © 2020 Franz Henri de Guzman. All rights reserved.
//

import UIKit

class View: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)

        setViews()
        layoutViews()
        addValuesToViews()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        setViews()
        layoutViews()
        addValuesToViews()
    }

    /// Set your view and its subviews here.
    func setViews() {
        backgroundColor = .white
    }

    /// Layout your subviews here.
    func layoutViews() {}
    
    /// Layout your subviews here.
    func addValuesToViews() {}

}
