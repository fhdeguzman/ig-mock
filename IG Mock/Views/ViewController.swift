//
//  ViewController.swift
//  IG Mock
//
//  Created by Franz Henri De Guzman on 10/26/20.
//  Copyright © 2020 Franz Henri de Guzman. All rights reserved.
//

import UIKit

class ViewController<V: View>: UIViewController {

    override func loadView() {
        view = V()
    }

    var customView: V {
        return view as! V
    }

}
