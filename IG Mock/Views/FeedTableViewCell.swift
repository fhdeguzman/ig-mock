//
//  FeedTableViewCell.swift
//  IG Mock
//
//  Created by Franz Henri de Guzman on 10/21/20.
//  Copyright © 2020 Franz Henri de Guzman. All rights reserved.
//

import UIKit

class FeedTableViewCell: UITableViewCell {
    
    let userImageview: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = UIColor.white
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = .black
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    
    let feedImageview: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = UIColor.white
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    
    let moreButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "more"), for: .normal)
        return button
    }()
    
    
    let actionContainer: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 0.96, green: 0.96, blue: 0.96, alpha: 1.00)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let likeButton: UIButton = {
           let button = UIButton()
           button.translatesAutoresizingMaskIntoConstraints = false
           button.setImage(UIImage(named: "like"), for: .normal)
           return button
    }()
    
    let commentButton: UIButton = {
           let button = UIButton()
           button.translatesAutoresizingMaskIntoConstraints = false
           button.setImage(UIImage(named: "comment"), for: .normal)
           return button
    }()
    
    
    let shareButton: UIButton = {
           let button = UIButton()
           button.translatesAutoresizingMaskIntoConstraints = false
           button.setImage(UIImage(named: "share"), for: .normal)
           return button
    }()
    
    let addToFavoriteButton: UIButton = {
           let button = UIButton()
           button.translatesAutoresizingMaskIntoConstraints = false
           button.setImage(UIImage(named: "bookmark"), for: .normal)
           return button
    }()
    
    let feedLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(red: 0.06, green: 0.06, blue: 0.06, alpha: 1.00)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        return label
    }()
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.addViews()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func addViews(){
        addSubview(userImageview)
        addSubview(moreButton)
        addSubview(nameLabel)
        addSubview(feedImageview)
        addSubview(actionContainer)
        addSubview(feedLabel)
        
        userImageview.leftAnchor.constraint(equalTo: leftAnchor, constant: 10).isActive = true
        userImageview.topAnchor.constraint(equalTo: topAnchor, constant: 10).isActive = true
        userImageview.heightAnchor.constraint(equalToConstant: 50).isActive = true
        userImageview.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
        moreButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        moreButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        moreButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        moreButton.centerYAnchor.constraint(equalTo: userImageview.centerYAnchor).isActive = true
        
        
        nameLabel.rightAnchor.constraint(equalTo: moreButton.leftAnchor, constant: -15).isActive = true
        nameLabel.leftAnchor.constraint(equalTo: userImageview.rightAnchor, constant: 15).isActive = true
        nameLabel.centerYAnchor.constraint(equalTo: userImageview.centerYAnchor).isActive = true
        nameLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        
        feedImageview.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
        feedImageview.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        feedImageview.topAnchor.constraint(equalTo: userImageview.bottomAnchor, constant: 10).isActive = true
        feedImageview.heightAnchor.constraint(equalToConstant: 400).isActive = true
        
        actionContainer.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
        actionContainer.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        actionContainer.topAnchor.constraint(equalTo: feedImageview.bottomAnchor, constant: 0).isActive = true
        actionContainer.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        feedLabel.topAnchor.constraint(equalTo: actionContainer.bottomAnchor, constant: 5).isActive = true
        feedLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 15).isActive = true
        feedLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -15).isActive = true
        feedLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -20).isActive = true
        
        actionContainer.addSubview(likeButton)
        likeButton.leftAnchor.constraint(equalTo: actionContainer.leftAnchor, constant: 10).isActive = true
        likeButton.centerYAnchor.constraint(equalTo: actionContainer.centerYAnchor).isActive = true
        likeButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        likeButton.widthAnchor.constraint(equalToConstant: 25).isActive = true
        
        
        actionContainer.addSubview(commentButton)
        commentButton.leftAnchor.constraint(equalTo: likeButton.rightAnchor, constant: 20).isActive = true
        commentButton.centerYAnchor.constraint(equalTo: actionContainer.centerYAnchor).isActive = true
        commentButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        commentButton.widthAnchor.constraint(equalToConstant: 25).isActive = true
        
        actionContainer.addSubview(shareButton)
        shareButton.leftAnchor.constraint(equalTo: commentButton.rightAnchor, constant: 20).isActive = true
        shareButton.centerYAnchor.constraint(equalTo: actionContainer.centerYAnchor).isActive = true
        shareButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        shareButton.widthAnchor.constraint(equalToConstant: 25).isActive = true
        
        actionContainer.addSubview(addToFavoriteButton)
        addToFavoriteButton.rightAnchor.constraint(equalTo: actionContainer.rightAnchor, constant: -10).isActive = true
        addToFavoriteButton.centerYAnchor.constraint(equalTo: actionContainer.centerYAnchor).isActive = true
        addToFavoriteButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        addToFavoriteButton.widthAnchor.constraint(equalToConstant: 25).isActive = true
        
    }
    
}
