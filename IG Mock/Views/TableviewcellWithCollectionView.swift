//
//  TableviewcellWithCollectionView.swift
//  IG Mock
//
//  Created by Franz Henri De Guzman on 10/28/20.
//  Copyright © 2020 Franz Henri de Guzman. All rights reserved.
//

import Foundation
import UIKit


class TableviewcellWithCollectionView: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {


    var mockImg: [String] = ["user", "f1", "f2", "f3", "f4", "f4"]
    var mockName: [String] = ["Professor", "Tokyo", "Nairobi", "Helsinki", "Berlin", "Berlin"]

    var collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout.init())

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        

        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.scrollDirection = .horizontal
        layout.itemSize = CGSize(width: 90, height: 90)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        collectionView.setNeedsLayout()
        collectionView.frame = CGRect(x: 0, y: 0, width: 90 * mockImg.count + 10, height: 100)
        collectionView.setCollectionViewLayout(layout, animated: true)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(StoryCollectionViewCell.self, forCellWithReuseIdentifier: "cell")

        collectionView.setCollectionViewLayout(layout, animated: true)
        collectionView.backgroundColor = .red
        collectionView.bounces = true
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.isScrollEnabled = true


        self.backgroundColor = .brown
        self.addSubview(collectionView)

        self.layoutIfNeeded()
        self.setNeedsLayout()
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }

    // MARK: UICollectionViewDataSource
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }


    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mockImg.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let svcell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! StoryCollectionViewCell
        svcell.nameLabel.text = mockName[indexPath.row]
        svcell.storyImageview.image = UIImage(named: mockImg[indexPath.row])
        svcell.storyImageview.layer.masksToBounds = true
        svcell.storyImageview.layer.cornerRadius = svcell.frame.height * 3/10
        return svcell
    }

}
