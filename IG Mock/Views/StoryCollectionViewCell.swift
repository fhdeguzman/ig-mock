//
//  StoryCollectionViewCell.swift
//  IG Mock
//
//  Created by Franz Henri de Guzman on 10/20/20.
//  Copyright © 2020 Franz Henri de Guzman. All rights reserved.
//

import UIKit

class StoryCollectionViewCell: UICollectionViewCell {
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = UIColor.darkGray
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let storyImageview: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = UIColor.white
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.contentView.isUserInteractionEnabled = false
        addViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    func addViews(){
        addSubview(storyImageview)
        addSubview(nameLabel)
        
        storyImageview.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        storyImageview.topAnchor.constraint(equalTo: topAnchor, constant: 10).isActive = true
        storyImageview.heightAnchor.constraint(equalToConstant: self.frame.height * 3/5).isActive = true
        storyImageview.widthAnchor.constraint(equalToConstant: self.frame.width * 3/5).isActive = true
        
        
        nameLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
        nameLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        nameLabel.topAnchor.constraint(equalTo: storyImageview.bottomAnchor, constant: 0).isActive = true
        nameLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 3).isActive = true
        
    }

    
}
