//
//  PostsCollectionViewCell.swift
//  IG Mock
//
//  Created by Franz Henri De Guzman on 10/22/20.
//  Copyright © 2020 Franz Henri de Guzman. All rights reserved.
//

import UIKit

class PostsCollectionViewCell: UICollectionViewCell {
    
    let postImageview: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = UIColor.white
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    func addViews(){
        addSubview(postImageview)
        
       
        postImageview.topAnchor.constraint(equalTo: topAnchor, constant: 0).isActive = true
        postImageview.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
        postImageview.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
        postImageview.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        
        
    }
    
}
