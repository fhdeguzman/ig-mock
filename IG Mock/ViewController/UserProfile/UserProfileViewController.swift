//
//  UserProfileViewController.swift
//  IG Mock
//
//  Created by Franz Henri De Guzman on 10/26/20.
//  Copyright © 2020 Franz Henri de Guzman. All rights reserved.
//

import Foundation
import UIKit


class UserProfileViewController: ViewController<UserProfileView> {


    var upv = UserProfileView()

    override func viewDidLoad() {
        super.viewDidLoad()

        upv.scrollView.updateContentView()
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setUpNavigationBar()

    }

    
    private func setUpNavigationBar() {
        //MARK: ADD IG NAME LOGO AS TITLE VIEW
        self.tabBarController?.navigationItem.titleView = nil
        self.tabBarController?.navigationItem.title = "Professor"

        //MARK: ADD LEFT BAR BUTTON THAT OPENS CAMERA
        let leftbutton: UIButton = UIButton(type: .custom)
        leftbutton.setImage(UIImage(named: "add"), for: .normal)
        leftbutton.addTarget(self, action: #selector(self.addPost), for: .touchUpInside)
        let leftBarButton = UIBarButtonItem(customView: leftbutton)
        self.tabBarController!.navigationItem.leftBarButtonItem = leftBarButton


        //MARK: ADD RIGHT BAR BUTTON
        let rightbutton: UIButton = UIButton(type: .custom)
        rightbutton.setImage(UIImage(named: "menu"), for: .normal)
        rightbutton.addTarget(self, action: #selector(self.menu), for: .touchUpInside)
        let rightBarButton = UIBarButtonItem(customView: rightbutton)
        self.tabBarController!.navigationItem.rightBarButtonItem = rightBarButton

    }

    
    @objc func addPost() {
        print("Open Camera")
    }

    @objc func menu() {
        print("Open Message")
    }

    
}
