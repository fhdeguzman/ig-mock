//
//  UserProfileView.swift
//  IG Mock
//
//  Created by Franz Henri De Guzman on 10/26/20.
//  Copyright © 2020 Franz Henri de Guzman. All rights reserved.
//

import Foundation
import UIKit


class UserProfileView: View, UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == highlightsCollectionView {
            return 1
        }else{
            return mockImg.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == highlightsCollectionView {
            let svcell = highlightsCollectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! StoryCollectionViewCell
            svcell.nameLabel.text = "New"
            svcell.storyImageview.image = UIImage(named: "add")
            svcell.storyImageview.layer.borderWidth = 1
            svcell.storyImageview.layer.borderColor = UIColor(red: 0.08, green: 0.08, blue: 0.08, alpha: 1.00).cgColor
            svcell.storyImageview.layer.masksToBounds = true
            svcell.storyImageview.layer.cornerRadius = svcell.frame.height * 3/10
            return svcell
        }else{
            let svcell = postsCollectionView.dequeueReusableCell(withReuseIdentifier: "pcell", for: indexPath) as! PostsCollectionViewCell
            svcell.postImageview.image = UIImage(named: mockImg[indexPath.row])
            return svcell
        }
    }
    
    var mockImg: [String] = ["user", "f1", "f2", "f3", "f4", "f1", "f2", "f3", "f4", "f1", "f2", "f3", "f4", "f1"]
    
    
    
    let profileContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let profileImageview: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "user")
        imageView.layer.masksToBounds = true
        imageView.layer.cornerRadius = (UIScreen.main.bounds.height/8 - 25) * 1/2
        return imageView
    }()
    
    let numberOfPostContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    
    let postLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(red: 0.08, green: 0.08, blue: 0.08, alpha: 1.00)
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        return label
    }()
    
    let numberOfFollowersContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let followersLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(red: 0.08, green: 0.08, blue: 0.08, alpha: 1.00)
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        return label
    }()
    
    
    let numberOfFollowingContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    
    let followingLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(red: 0.08, green: 0.08, blue: 0.08, alpha: 1.00)
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        return label
    }()
    
    let nameContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let usernameLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(red: 0.08, green: 0.08, blue: 0.08, alpha: 1.00)
        label.textAlignment = .left
        label.font = UIFont(name: "HelveticaNeue-Medium", size: 16.0)!
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let descriptionLabel: UILabel = {
        let label = UILabel()
        label.textColor = .darkGray
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        return label
    }()
    
    
    let editProfileButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(UIColor(red: 0.08, green: 0.08, blue: 0.08, alpha: 1.00), for: .normal)
        button.layer.cornerRadius = 3
        button.layer.borderWidth = 1
        let buttonTitleAttributes = [
            NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Medium", size: 14.0)!
        ]
        let buttonTitle = NSAttributedString(string: "Edit Profile", attributes: buttonTitleAttributes as [NSAttributedString.Key : Any])
        button.setAttributedTitle(buttonTitle, for: .normal)
        
        button.layer.borderColor = UIColor.lightGray.cgColor
        return button
    }()
    
    let highlightsContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .lightGray
        return view
    }()
    
    let postsContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let scrollView: UIScrollView = {
        let sv = UIScrollView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    
    
    var highlightsCollectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout.init())
    
    var postsCollectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout.init())
    
    override func setViews() {
        super.setViews()
        addSubview(scrollView)
        scrollView.addSubview(profileContainer)
        scrollView.addSubview(nameContainer)
        scrollView.addSubview(editProfileButton)
        scrollView.addSubview(highlightsContainer)
        scrollView.addSubview(postsContainer)
        profileContainer.addSubview(profileImageview)
        profileContainer.addSubview(numberOfPostContainer)
        numberOfPostContainer.addSubview(postLabel)
        profileContainer.addSubview(numberOfFollowersContainer)
        numberOfFollowersContainer.addSubview(followersLabel)
        profileContainer.addSubview(numberOfFollowingContainer)
        numberOfFollowingContainer.addSubview(followingLabel)
        nameContainer.addSubview(usernameLabel)
        nameContainer.addSubview(descriptionLabel)
        highlightsContainer.addSubview(self.highlightsCollectionView)
        postsContainer.addSubview(self.postsCollectionView)
    }
    
    override func layoutViews() {
        scrollView.contentSize = CGSize(width: self.frame.width, height:1500)
        scrollView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 0).isActive = true
        scrollView.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        scrollView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        
        //MARK: Profile Container
        profileContainer.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10).isActive = true
        profileContainer.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0).isActive = true
        profileContainer.heightAnchor.constraint(equalToConstant: (UIScreen.main.bounds.height)/8).isActive = true
        profileContainer.widthAnchor.constraint(equalToConstant: (UIScreen.main.bounds.width)).isActive = true
        
        //MARK: Add User Image in Profile Container
        profileImageview.leftAnchor.constraint(equalTo: profileContainer.leftAnchor, constant: 10).isActive = true
        profileImageview.centerYAnchor.constraint(equalTo: profileContainer.centerYAnchor).isActive = true
        profileImageview.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.height/8 - 25 ).isActive = true
        profileImageview.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.height/8 - 25 ).isActive = true
        
        //MARK: Add Number of Posts in Profile Container
        numberOfPostContainer.leftAnchor.constraint(equalTo: profileImageview.rightAnchor, constant: 3).isActive = true
        numberOfPostContainer.topAnchor.constraint(equalTo: profileContainer.topAnchor, constant: 0).isActive = true
        numberOfPostContainer.bottomAnchor.constraint(equalTo: profileContainer.bottomAnchor, constant: 0).isActive = true
        numberOfPostContainer.widthAnchor.constraint(equalToConstant: (UIScreen.main.bounds.width - 20 - (UIScreen.main.bounds.height/8 - 25)) * 1/3).isActive = true
        
        postLabel.leftAnchor.constraint(equalTo: numberOfPostContainer.leftAnchor, constant: 3).isActive = true
        postLabel.rightAnchor.constraint(equalTo: numberOfPostContainer.rightAnchor, constant: -3).isActive = true
        postLabel.topAnchor.constraint(equalTo: numberOfPostContainer.topAnchor, constant: 3).isActive = true
        postLabel.bottomAnchor.constraint(equalTo: numberOfPostContainer.bottomAnchor, constant: 3).isActive = true
        
        //MARK: Add Number of Followers in Profile Container
        numberOfFollowersContainer.leftAnchor.constraint(equalTo: numberOfPostContainer.rightAnchor, constant: 0).isActive = true
        numberOfFollowersContainer.topAnchor.constraint(equalTo: profileContainer.topAnchor, constant: 0).isActive = true
        numberOfFollowersContainer.bottomAnchor.constraint(equalTo: profileContainer.bottomAnchor, constant: 0).isActive = true
        let x = UIScreen.main.bounds.width - 20
        let y = (UIScreen.main.bounds.height/8 - 25)
        numberOfFollowersContainer.widthAnchor.constraint(equalToConstant: ( x - y ) * 1/3).isActive = true
        
        followersLabel.leftAnchor.constraint(equalTo: numberOfFollowersContainer.leftAnchor, constant: 3).isActive = true
        followersLabel.rightAnchor.constraint(equalTo: numberOfFollowersContainer.rightAnchor, constant: -3).isActive = true
        followersLabel.topAnchor.constraint(equalTo: numberOfFollowersContainer.topAnchor, constant: 3).isActive = true
        followersLabel.bottomAnchor.constraint(equalTo: numberOfFollowersContainer.bottomAnchor, constant: 3).isActive = true
        
        //MARK: Add Number of Following in Profile Container
        profileContainer.addSubview(numberOfFollowingContainer)
        numberOfFollowingContainer.leftAnchor.constraint(equalTo: numberOfFollowersContainer.rightAnchor, constant: 0).isActive = true
        numberOfFollowingContainer.rightAnchor.constraint(equalTo: profileContainer.rightAnchor, constant: -3).isActive = true
        numberOfFollowingContainer.topAnchor.constraint(equalTo: profileContainer.topAnchor, constant: 0).isActive = true
        numberOfFollowingContainer.bottomAnchor.constraint(equalTo: profileContainer.bottomAnchor, constant: 0).isActive = true
        
        followingLabel.leftAnchor.constraint(equalTo: numberOfFollowingContainer.leftAnchor, constant: 3).isActive = true
        followingLabel.rightAnchor.constraint(equalTo: numberOfFollowingContainer.rightAnchor, constant: -20).isActive = true
        followingLabel.topAnchor.constraint(equalTo: numberOfFollowingContainer.topAnchor, constant: 3).isActive = true
        followingLabel.bottomAnchor.constraint(equalTo: numberOfFollowingContainer.bottomAnchor, constant: 3).isActive = true
        
        
        //MARK: Details Container
        nameContainer.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        nameContainer.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        nameContainer.topAnchor.constraint(equalTo: profileContainer.bottomAnchor, constant: 0).isActive = true
        nameContainer.bottomAnchor.constraint(equalTo: editProfileButton.topAnchor, constant: 0).isActive = true
        
        //MARK: Add Username in Details Container
        usernameLabel.leftAnchor.constraint(equalTo: nameContainer.leftAnchor, constant: 20).isActive = true
        usernameLabel.rightAnchor.constraint(equalTo: nameContainer.rightAnchor, constant: -10).isActive = true
        usernameLabel.topAnchor.constraint(equalTo: nameContainer.topAnchor, constant: 3).isActive = true
        usernameLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        //MARK: Add Description in Details Container
        descriptionLabel.leftAnchor.constraint(equalTo: nameContainer.leftAnchor, constant: 20).isActive = true
        descriptionLabel.rightAnchor.constraint(equalTo: nameContainer.rightAnchor, constant: -20).isActive = true
        descriptionLabel.topAnchor.constraint(equalTo: usernameLabel.bottomAnchor, constant: 1).isActive = true
        descriptionLabel.bottomAnchor.constraint(equalTo: nameContainer.bottomAnchor, constant: -20).isActive = true
        
        
        //MARK: Edit Profile Button
        editProfileButton.topAnchor.constraint(equalTo: nameContainer.bottomAnchor, constant: 0).isActive = true
        editProfileButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        editProfileButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -20).isActive = true
        editProfileButton.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 20).isActive = true
        
        //MARK: Highlights Container
        highlightsContainer.topAnchor.constraint(equalTo: editProfileButton.bottomAnchor, constant: 2).isActive = true
        highlightsContainer.heightAnchor.constraint(equalToConstant: 80).isActive = true
        highlightsContainer.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10).isActive = true
        highlightsContainer.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10).isActive = true
        
        //MARK: Posts Collection View Container
        postsContainer.topAnchor.constraint(equalTo: highlightsContainer.bottomAnchor, constant: 8).isActive = true
        postsContainer.heightAnchor.constraint(equalToConstant: 700).isActive = true
        postsContainer.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 0).isActive = true
        postsContainer.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0).isActive = true
    }
    
    
    override func addValuesToViews() {
        let boldAttribute = [
            NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Medium", size: 20.0)!
        ]
        
        let regularAttribute = [
            NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Light", size: 14.0)!
        ]
        
        let numberOfPost = NSAttributedString(string: "191", attributes: boldAttribute as [NSAttributedString.Key : Any])
        let post = NSAttributedString(string: "  \nPosts", attributes: regularAttribute)
        
        let newpostLabel = NSMutableAttributedString()
        newpostLabel.append(numberOfPost)
        newpostLabel.append(post)
        postLabel.attributedText = newpostLabel
        
        
        
        let numberOfFollowers = NSAttributedString(string: "697", attributes: boldAttribute as [NSAttributedString.Key : Any])
        let followers = NSAttributedString(string: "  \nFollowers", attributes: regularAttribute)
        
        let newfollowersLabel = NSMutableAttributedString()
        newfollowersLabel.append(numberOfFollowers)
        newfollowersLabel.append(followers)
        followersLabel.attributedText = newfollowersLabel
        
        
        let numberOfFollowing = NSAttributedString(string: "36", attributes: boldAttribute as [NSAttributedString.Key : Any])
        let following = NSAttributedString(string: "  \nFollowing", attributes: regularAttribute)
        
        let newfollowingLabel = NSMutableAttributedString()
        newfollowingLabel.append(numberOfFollowing)
        newfollowingLabel.append(following)
        followingLabel.attributedText = newfollowingLabel
        
        
        usernameLabel.text = "Franz Henri de Guzman"
        descriptionLabel.text = "Lorem ipsum dolor sit amet, sale graece imperdiet vis in. Has verterem patrioque ullamcorper at, an pro unum tacimates vulputate. \nEst utroque conceptam at, ad nemore persius quo. Ex quas facete everti mea."
        
        
        highlightsCollectionView.delegate = self
        highlightsCollectionView.dataSource = self
        highlightsCollectionView.register(StoryCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        
        let hlayout:UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
        hlayout.sectionInset = UIEdgeInsets(top: 0, left: 3, bottom: 0, right: 3)
        hlayout.scrollDirection = .horizontal
        highlightsCollectionView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 80)
        hlayout.itemSize = CGSize(width: 80, height: 80)
        hlayout.minimumInteritemSpacing = 0
        hlayout.minimumLineSpacing = 0
        highlightsCollectionView.setCollectionViewLayout(hlayout, animated: true)
        highlightsCollectionView.backgroundColor = .white
        highlightsCollectionView.bounces = true
        highlightsCollectionView.showsHorizontalScrollIndicator = false

        
        postsCollectionView.delegate = self
        postsCollectionView.dataSource = self
        postsCollectionView.register(PostsCollectionViewCell.self, forCellWithReuseIdentifier: "pcell")
        postsCollectionView.backgroundColor = .white
        let playout:UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
        playout.sectionInset = UIEdgeInsets(top: 2, left: 2, bottom: -2, right: 0)
        playout.scrollDirection = .vertical
        playout.itemSize = CGSize(width: (UIScreen.main.bounds.width/3.0) - 4, height: (UIScreen.main.bounds.width/3.0) - 2)
        playout.minimumInteritemSpacing = 0
        playout.minimumLineSpacing = 2
        
        let x: Double = Double(mockImg.count) / Double(3)
        postsCollectionView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 2, height: playout.itemSize.height * ceil(CGFloat(x)) + 25)
        
        postsCollectionView.setNeedsLayout()
        postsCollectionView.setCollectionViewLayout(playout, animated: true)
        postsCollectionView.bounces = true
        postsCollectionView.showsHorizontalScrollIndicator = false
        postsCollectionView.isScrollEnabled = false

        scrollView.showsVerticalScrollIndicator = false

    }
    
    
    
}
