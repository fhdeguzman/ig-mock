//
//  IGFeedViewController.swift
//  IG Mock
//
//  Created by Franz Henri de Guzman on 10/19/20.
//  Copyright © 2020 Franz Henri de Guzman. All rights reserved.
//

import UIKit


extension IGFeedViewController : UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //MARK: Select items
    }
}


extension IGFeedViewController : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let svcell = storyCollectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! StoryCollectionViewCell
        svcell.nameLabel.text = mockName[indexPath.row]
        svcell.storyImageview.image = UIImage(named: mockImg[indexPath.row])
        svcell.storyImageview.layer.masksToBounds = true
        svcell.storyImageview.layer.cornerRadius = svcell.frame.height * 3/10
        return svcell
    }
}

extension IGFeedViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Num: \(indexPath.row)")
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

extension IGFeedViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath as IndexPath) as! FeedTableViewCell
        cell.userImageview.image = UIImage(named: mockImg[indexPath.row])
        cell.userImageview.layer.masksToBounds = true
        cell.userImageview.layer.cornerRadius = 25
        cell.nameLabel.text = mockName[indexPath.row]
        cell.feedImageview.image = UIImage(named: mockImg[indexPath.row])
        cell.selectionStyle = .none
        let boldText = NSAttributedString(string: mockName[indexPath.row], attributes: boldAttribute as [NSAttributedString.Key : Any])
        let regularText = NSAttributedString(string: "  Lorem ipsum dolor sit amet, sale graece imperdiet vis in. Has verterem patrioque ullamcorper at, an pro unum tacimates vulputate. Est utroque conceptam at, ad nemore persius quo. Ex quas facete everti mea.", attributes: regularAttribute)
        
        let newString = NSMutableAttributedString()
        newString.append(boldText)
        newString.append(regularText)
        
        cell.feedLabel.attributedText = newString
        return cell
    }
    
    
}

class IGFeedViewController: UIViewController, UICollectionViewDelegateFlowLayout, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    
    let storyCollectionviewContainer: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    
    var storyCollectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout.init())
    
    private var feedTableView = UITableView()
    
    
    let divider: UIView = {
        let view = UIView()
        view.backgroundColor = .systemGray6
        return view
    }()
    
    
    var mockImg: [String] = ["user", "f1", "f2", "f3", "f4"]
    var mockName: [String] = ["Professor", "Tokyo", "Nairobi", "Helsinki", "Berlin"]
    
    let boldAttribute = [
        NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Bold", size: 14.0)!
    ]
    
    let regularAttribute = [
        NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Light", size: 16.0)!
    ]

    let scrollView: UIScrollView = {
        let sv = UIScrollView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white

        self.view.addSubview(scrollView)
        
        self.storyCollectionView.delegate = self
        self.storyCollectionView.dataSource = self
        self.storyCollectionView.register(StoryCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        
        self.setUpStoryCollectionViewContainerConstraint()
        self.setUpDividerConstraint()
        
        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.scrollDirection = .horizontal
        self.storyCollectionView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 100)
        layout.itemSize = CGSize(width: 90, height: 90)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        self.storyCollectionView.setCollectionViewLayout(layout, animated: true)
        self.storyCollectionView.backgroundColor = .white
        self.storyCollectionView.bounces = true
        self.storyCollectionView.showsHorizontalScrollIndicator = false
        self.storyCollectionviewContainer.addSubview(self.storyCollectionView)
        
        
        feedTableView.register(FeedTableViewCell.self, forCellReuseIdentifier: "MyCell")
        feedTableView.translatesAutoresizingMaskIntoConstraints = false
        feedTableView.showsVerticalScrollIndicator = false
        feedTableView.dataSource = self
        feedTableView.delegate = self
        feedTableView.estimatedRowHeight = 300
        feedTableView.rowHeight = UITableView.automaticDimension
        setFeedTableview()

        imagePicker.delegate = self
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(_:)))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        self.view.addGestureRecognizer(swipeRight)
    }

    let imagePicker: UIImagePickerController! = UIImagePickerController()

    @objc func respondToSwipeGesture(_ gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {

            switch swipeGesture.direction {

                case .right:

                    if ( UIImagePickerController.isSourceTypeAvailable(.camera)){
                        if UIImagePickerController.availableCaptureModes(for: .rear) != nil {
                            imagePicker.allowsEditing = false
                            imagePicker.sourceType = .camera
                            imagePicker.cameraCaptureMode = .photo
                            present(imagePicker,animated: true, completion: {})
                        }
                    }
                    break
                default:
                    break
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setUpNavigationBar()
    }
    
    
    private func setFeedTableview() {
        self.view.addSubview(self.feedTableView)
        self.feedTableView.translatesAutoresizingMaskIntoConstraints = false
        self.feedTableView.heightAnchor.constraint(
            equalToConstant: view.frame.height * 4/5 + 10).isActive = true
        self.feedTableView.leftAnchor.constraint(
            equalTo: view.leftAnchor).isActive = true
        self.feedTableView.rightAnchor.constraint(
            equalTo: view.rightAnchor).isActive = true
        self.feedTableView.topAnchor.constraint(equalTo: divider.bottomAnchor, constant: 0).isActive = true
        self.feedTableView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
        
    }
    
    
    private func setUpNavigationBar() {
        //MARK: ADD IG NAME LOGO AS TITLE VIEW
        let titleView = UIImageView()
        titleView.image = UIImage(named: "ig")
        titleView.contentMode = .scaleAspectFit
        self.tabBarController?.navigationItem.titleView = titleView
        
        //MARK: ADD LEFT BAR BUTTON THAT OPENS CAMERA
        let leftbutton: UIButton = UIButton(type: .custom)
        leftbutton.setImage(UIImage(named: "camera"), for: .normal)
        leftbutton.addTarget(self, action: #selector(self.openCamera), for: .touchUpInside)
        let leftBarButton = UIBarButtonItem(customView: leftbutton)
        self.tabBarController!.navigationItem.leftBarButtonItem = leftBarButton
        
        
        //MARK: ADD RIGHT BAR BUTTON
        let rightbutton: UIButton = UIButton(type: .custom)
        rightbutton.setImage(UIImage(named: "dm"), for: .normal)
        rightbutton.addTarget(self, action: #selector(self.openMessage), for: .touchUpInside)
        let rightBarButton = UIBarButtonItem(customView: rightbutton)
        self.tabBarController!.navigationItem.rightBarButtonItem = rightBarButton
        
    }
    
    
    @objc func openCamera() {
        if ( UIImagePickerController.isSourceTypeAvailable(.camera)){
            if UIImagePickerController.availableCaptureModes(for: .rear) != nil {
                imagePicker.allowsEditing = false
                imagePicker.sourceType = .camera
                imagePicker.cameraCaptureMode = .photo
                present(imagePicker,animated: true, completion: {})
            }
        }
    }
    
    @objc func openMessage() {
        print("Open Message")
    }
    
    
    func setUpStoryCollectionViewContainerConstraint(){
        self.view.addSubview(self.storyCollectionviewContainer)
        self.storyCollectionviewContainer.translatesAutoresizingMaskIntoConstraints = false
        self.storyCollectionviewContainer.heightAnchor.constraint(
            equalToConstant: 100).isActive = true
        self.storyCollectionviewContainer.leftAnchor.constraint(
            equalTo: view.leftAnchor).isActive = true
        self.storyCollectionviewContainer.rightAnchor.constraint(
            equalTo: view.rightAnchor).isActive = true
        self.storyCollectionviewContainer.topAnchor.constraint(
            equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        
        
    }
    
    private func setUpDividerConstraint() {
        self.view.addSubview(self.divider)
        self.divider.translatesAutoresizingMaskIntoConstraints = false
        self.divider.heightAnchor.constraint(
            equalToConstant: 1).isActive = true
        self.divider.leftAnchor.constraint(
            equalTo: view.leftAnchor).isActive = true
        self.divider.rightAnchor.constraint(
            equalTo: view.rightAnchor).isActive = true
        self.divider.topAnchor.constraint(
            equalTo: self.storyCollectionviewContainer.bottomAnchor).isActive = true
    }
    
}
