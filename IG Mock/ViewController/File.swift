//
//  UserProfileViewController.swift
//  IG Mock
//
//  Created by Franz Henri de Guzman on 10/20/20.
//  Copyright © 2020 Franz Henri de Guzman. All rights reserved.
//

import UIKit

extension xUserProfileViewController : UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //MARK: Select items
    }
}


extension xUserProfileViewController : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.highlightsCollectionView {
            return 1
        }else{
            return 5
        }

    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        if collectionView == self.highlightsCollectionView {
            let svcell = highlightsCollectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! StoryCollectionViewCell
            svcell.nameLabel.text = "New"
            svcell.storyImageview.image = UIImage(named: "add")
            svcell.storyImageview.layer.borderWidth = 1
            svcell.storyImageview.layer.borderColor = UIColor(red: 0.08, green: 0.08, blue: 0.08, alpha: 1.00).cgColor
            svcell.storyImageview.layer.masksToBounds = true
            svcell.storyImageview.layer.cornerRadius = svcell.frame.height * 3/10
            return svcell
        }else{
            let svcell = postsCollectionView.dequeueReusableCell(withReuseIdentifier: "pcell", for: indexPath) as! PostsCollectionViewCell
            svcell.postImageview.image = UIImage(named: self.mockImg[indexPath.row])
            return svcell
        }

        //        let svcell = highlightsCollectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! StoryCollectionViewCell
        //                   svcell.nameLabel.text = "New"
        //                   svcell.storyImageview.image = UIImage(named: "add")
        //                   svcell.storyImageview.layer.borderWidth = 1
        //                   svcell.storyImageview.layer.borderColor = UIColor(red: 0.08, green: 0.08, blue: 0.08, alpha: 1.00).cgColor
        //                   svcell.storyImageview.layer.masksToBounds = true
        //                   svcell.storyImageview.layer.cornerRadius = svcell.frame.height * 3/10
        //                   return svcell


    }
}

class xUserProfileViewController: UIViewController {

    let profileContainer: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    let profileImageview: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "user")
        return imageView
    }()

    let numberOfPostContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()


    let postLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(red: 0.08, green: 0.08, blue: 0.08, alpha: 1.00)
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        return label
    }()

    let numberOfFollowersContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    let followersLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(red: 0.08, green: 0.08, blue: 0.08, alpha: 1.00)
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        return label
    }()


    let numberOfFollowingContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()


    let followingLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(red: 0.08, green: 0.08, blue: 0.08, alpha: 1.00)
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        return label
    }()

    let nameContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    let usernameLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(red: 0.08, green: 0.08, blue: 0.08, alpha: 1.00)
        label.textAlignment = .left
        label.font = UIFont(name: "HelveticaNeue-Medium", size: 16.0)!
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    let descriptionLabel: UILabel = {
        let label = UILabel()
        label.textColor = .darkGray
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        return label
    }()


    let editProfileButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(UIColor(red: 0.08, green: 0.08, blue: 0.08, alpha: 1.00), for: .normal)
        button.layer.cornerRadius = 3
        button.layer.borderWidth = 1
        let buttonTitleAttributes = [
            NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Medium", size: 14.0)!
        ]
        let buttonTitle = NSAttributedString(string: "Edit Profile", attributes: buttonTitleAttributes as [NSAttributedString.Key : Any])
        button.setAttributedTitle(buttonTitle, for: .normal)

        button.layer.borderColor = UIColor.lightGray.cgColor
        return button
    }()

    let highlightsContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    let postsContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    let scrollView: UIScrollView = {
        let sv = UIScrollView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()

    let boldAttribute = [
        NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Medium", size: 20.0)!
    ]

    let regularAttribute = [
        NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Light", size: 14.0)!
    ]

    var highlightsCollectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout.init())

    var postsCollectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout.init())

    var mockImg: [String] = ["user", "f1", "f2", "f3", "f4"]


    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white

        scrollView.contentSize = CGSize(width: view.frame.width, height:1000)

        self.profileImageview.layer.masksToBounds = true
        self.profileImageview.layer.cornerRadius = (view.frame.height/8 - 25) * 1/2


        let numberOfPost = NSAttributedString(string: "191", attributes: boldAttribute as [NSAttributedString.Key : Any])
        let post = NSAttributedString(string: "  \nPosts", attributes: regularAttribute)

        let newpostLabel = NSMutableAttributedString()
        newpostLabel.append(numberOfPost)
        newpostLabel.append(post)
        postLabel.attributedText = newpostLabel


        let numberOfFollowers = NSAttributedString(string: "697", attributes: boldAttribute as [NSAttributedString.Key : Any])
        let followers = NSAttributedString(string: "  \nFollowers", attributes: regularAttribute)

        let newfollowersLabel = NSMutableAttributedString()
        newfollowersLabel.append(numberOfFollowers)
        newfollowersLabel.append(followers)
        followersLabel.attributedText = newfollowersLabel


        let numberOfFollowing = NSAttributedString(string: "36", attributes: boldAttribute as [NSAttributedString.Key : Any])
        let following = NSAttributedString(string: "  \nFollowing", attributes: regularAttribute)

        let newfollowingLabel = NSMutableAttributedString()
        newfollowingLabel.append(numberOfFollowing)
        newfollowingLabel.append(following)
        followingLabel.attributedText = newfollowingLabel


        self.usernameLabel.text = "Franz Henri de Guzman"
        self.descriptionLabel.text = "Lorem ipsum dolor sit amet, sale graece imperdiet vis in. Has verterem patrioque ullamcorper at, an pro unum tacimates vulputate. \nEst utroque conceptam at, ad nemore persius quo. Ex quas facete everti mea."

        self.highlightsCollectionView.delegate = self
        self.highlightsCollectionView.dataSource = self
        self.highlightsCollectionView.register(StoryCollectionViewCell.self, forCellWithReuseIdentifier: "cell")

        let hlayout:UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
        hlayout.sectionInset = UIEdgeInsets(top: 0, left: 3, bottom: 0, right: 3)
        hlayout.scrollDirection = .horizontal
        self.highlightsCollectionView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height/5)
        hlayout.itemSize = CGSize(width: 80, height: 80)
        hlayout.minimumInteritemSpacing = 0
        hlayout.minimumLineSpacing = 0
        self.highlightsCollectionView.setCollectionViewLayout(hlayout, animated: true)
        self.highlightsCollectionView.backgroundColor = .white
        self.highlightsCollectionView.bounces = true
        self.highlightsCollectionView.showsHorizontalScrollIndicator = false
        self.highlightsContainer.addSubview(self.highlightsCollectionView)

        self.postsCollectionView.delegate = self
        self.postsCollectionView.dataSource = self
        self.postsCollectionView.register(PostsCollectionViewCell.self, forCellWithReuseIdentifier: "pcell")

        let playout:UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
        playout.sectionInset = UIEdgeInsets(top: 2, left: 2, bottom: -2, right: 0)
        playout.scrollDirection = .vertical


        self.postsCollectionView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 0)
        //self.postsCollectionView.heightAnchor.constraint(equalToConstant: (postsCollectionView.collectionViewLayout.collectionView?.contentSize.height)!).isActive = true
        self.view.setNeedsLayout()

        playout.itemSize = CGSize(width: (view.bounds.width/3.0) - 4, height: (view.bounds.width/3.0) - 2)
        playout.minimumInteritemSpacing = 0
        playout.minimumLineSpacing = 2
        let numRows = mockImg.count/3
        let rounded = CGFloat(numRows).rounded(.up)
       // let d : Double = Double(numRows) - Double(Int(numRows))
        self.postsCollectionView.frame = CGRect(x: 0, y: 0, width: view.frame.width - 2, height: playout.itemSize.height * 2)
        self.postsCollectionView.setNeedsLayout()
        self.postsCollectionView.setCollectionViewLayout(playout, animated: true)
        self.postsCollectionView.backgroundColor = .blue
        self.postsCollectionView.bounces = true
        self.postsCollectionView.showsHorizontalScrollIndicator = false
        self.postsCollectionView.isScrollEnabled = false
        self.postsContainer.addSubview(self.postsCollectionView)

    }


    override func viewDidLayoutSubviews() {
            print("This is run on the background queue")
            self.addViews()

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setUpNavigationBar()

    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = collectionView.bounds.width/3.0
        let yourHeight = yourWidth

        return CGSize(width: yourWidth, height: yourHeight)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    private func setUpNavigationBar() {
        //MARK: ADD IG NAME LOGO AS TITLE VIEW
        self.tabBarController?.navigationItem.titleView = nil
        self.tabBarController?.navigationItem.title = "Professor"

        //MARK: ADD LEFT BAR BUTTON THAT OPENS CAMERA
        let leftbutton: UIButton = UIButton(type: .custom)
        leftbutton.setImage(UIImage(named: "add"), for: .normal)
        leftbutton.addTarget(self, action: #selector(self.addPost), for: .touchUpInside)
        let leftBarButton = UIBarButtonItem(customView: leftbutton)
        self.tabBarController!.navigationItem.leftBarButtonItem = leftBarButton


        //MARK: ADD RIGHT BAR BUTTON
        let rightbutton: UIButton = UIButton(type: .custom)
        rightbutton.setImage(UIImage(named: "menu"), for: .normal)
        rightbutton.addTarget(self, action: #selector(self.menu), for: .touchUpInside)
        let rightBarButton = UIBarButtonItem(customView: rightbutton)
        self.tabBarController!.navigationItem.rightBarButtonItem = rightBarButton

    }


    @objc func addPost() {
        print("Open Camera")
    }

    @objc func menu() {
        print("Open Message")
    }


    private func addViews() {
        view.addSubview(scrollView)
        scrollView.addSubview(profileContainer)
        scrollView.addSubview(nameContainer)
        scrollView.addSubview(editProfileButton)
        scrollView.addSubview(highlightsContainer)
        scrollView.addSubview(postsContainer)

        scrollView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        scrollView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        scrollView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true

        //MARK: Profile Container
        self.profileContainer.leftAnchor.constraint(equalTo: scrollView.leftAnchor, constant: 10).isActive = true
        self.profileContainer.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0).isActive = true
        self.profileContainer.heightAnchor.constraint(equalToConstant: view.frame.height/8).isActive = true
        self.profileContainer.widthAnchor.constraint(equalToConstant: view.frame.width).isActive = true

        //MARK: Add User Image in Profile Container
        self.profileContainer.addSubview(profileImageview)
        self.profileImageview.leftAnchor.constraint(equalTo: profileContainer.leftAnchor, constant: 10).isActive = true
        self.profileImageview.centerYAnchor.constraint(equalTo: profileContainer.centerYAnchor).isActive = true
        self.profileImageview.heightAnchor.constraint(equalToConstant: view.frame.height/8 - 25 ).isActive = true
        self.profileImageview.widthAnchor.constraint(equalToConstant: view.frame.height/8 - 25 ).isActive = true

        //MARK: Add Number of Posts in Profile Container
        self.profileContainer.addSubview(numberOfPostContainer)
        self.numberOfPostContainer.leftAnchor.constraint(equalTo: profileImageview.rightAnchor, constant: 3).isActive = true
        self.numberOfPostContainer.topAnchor.constraint(equalTo: profileContainer.topAnchor, constant: 0).isActive = true
        self.numberOfPostContainer.bottomAnchor.constraint(equalTo: profileContainer.bottomAnchor, constant: 0).isActive = true
        self.numberOfPostContainer.widthAnchor.constraint(equalToConstant: (view.frame.width - 20 - (view.frame.height/8 - 25)) * 1/3).isActive = true

        self.numberOfPostContainer.addSubview(postLabel)
        self.postLabel.leftAnchor.constraint(equalTo: numberOfPostContainer.leftAnchor, constant: 3).isActive = true
        self.postLabel.rightAnchor.constraint(equalTo: numberOfPostContainer.rightAnchor, constant: -3).isActive = true
        self.postLabel.topAnchor.constraint(equalTo: numberOfPostContainer.topAnchor, constant: 3).isActive = true
        self.postLabel.bottomAnchor.constraint(equalTo: numberOfPostContainer.bottomAnchor, constant: 3).isActive = true

        //MARK: Add Number of Followers in Profile Container
        self.profileContainer.addSubview(numberOfFollowersContainer)
        self.numberOfFollowersContainer.leftAnchor.constraint(equalTo: numberOfPostContainer.rightAnchor, constant: 0).isActive = true
        self.numberOfFollowersContainer.topAnchor.constraint(equalTo: profileContainer.topAnchor, constant: 0).isActive = true
        self.numberOfFollowersContainer.bottomAnchor.constraint(equalTo: profileContainer.bottomAnchor, constant: 0).isActive = true
        self.numberOfFollowersContainer.widthAnchor.constraint(equalToConstant: (view.frame.width - 20 - (view.frame.height/8 - 25)) * 1/3).isActive = true

        self.numberOfFollowersContainer.addSubview(followersLabel)
        self.followersLabel.leftAnchor.constraint(equalTo: numberOfFollowersContainer.leftAnchor, constant: 3).isActive = true
        self.followersLabel.rightAnchor.constraint(equalTo: numberOfFollowersContainer.rightAnchor, constant: -3).isActive = true
        self.followersLabel.topAnchor.constraint(equalTo: numberOfFollowersContainer.topAnchor, constant: 3).isActive = true
        self.followersLabel.bottomAnchor.constraint(equalTo: numberOfFollowersContainer.bottomAnchor, constant: 3).isActive = true

        //MARK: Add Number of Following in Profile Container
        self.profileContainer.addSubview(numberOfFollowingContainer)
        self.numberOfFollowingContainer.leftAnchor.constraint(equalTo: numberOfFollowersContainer.rightAnchor, constant: 0).isActive = true
        self.numberOfFollowingContainer.rightAnchor.constraint(equalTo: profileContainer.rightAnchor, constant: -3).isActive = true
        self.numberOfFollowingContainer.topAnchor.constraint(equalTo: profileContainer.topAnchor, constant: 0).isActive = true
        self.numberOfFollowingContainer.bottomAnchor.constraint(equalTo: profileContainer.bottomAnchor, constant: 0).isActive = true

        self.numberOfFollowingContainer.addSubview(followingLabel)
        self.followingLabel.leftAnchor.constraint(equalTo: numberOfFollowingContainer.leftAnchor, constant: 3).isActive = true
        self.followingLabel.rightAnchor.constraint(equalTo: numberOfFollowingContainer.rightAnchor, constant: -20).isActive = true
        self.followingLabel.topAnchor.constraint(equalTo: numberOfFollowingContainer.topAnchor, constant: 3).isActive = true
        self.followingLabel.bottomAnchor.constraint(equalTo: numberOfFollowingContainer.bottomAnchor, constant: 3).isActive = true


        //MARK: Details Container
        self.nameContainer.leftAnchor.constraint(equalTo: scrollView.leftAnchor).isActive = true
        self.nameContainer.topAnchor.constraint(equalTo: profileContainer.bottomAnchor, constant: 0).isActive = true
        self.nameContainer.bottomAnchor.constraint(equalTo: editProfileButton.topAnchor, constant: 0).isActive = true
        self.nameContainer.widthAnchor.constraint(equalToConstant: view.frame.width).isActive = true

        //MARK: Add Username in Details Container
        self.nameContainer.addSubview(usernameLabel)
        self.usernameLabel.leftAnchor.constraint(equalTo: nameContainer.leftAnchor, constant: 20).isActive = true
        self.usernameLabel.rightAnchor.constraint(equalTo: nameContainer.rightAnchor, constant: -10).isActive = true
        self.usernameLabel.topAnchor.constraint(equalTo: nameContainer.topAnchor, constant: 3).isActive = true
        self.usernameLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true

        //MARK: Add Description in Details Container
        self.nameContainer.addSubview(descriptionLabel)
        self.descriptionLabel.leftAnchor.constraint(equalTo: nameContainer.leftAnchor, constant: 20).isActive = true
        self.descriptionLabel.rightAnchor.constraint(equalTo: nameContainer.rightAnchor, constant: -20).isActive = true
        self.descriptionLabel.topAnchor.constraint(equalTo: usernameLabel.bottomAnchor, constant: 1).isActive = true
        self.descriptionLabel.bottomAnchor.constraint(equalTo: nameContainer.bottomAnchor, constant: -20).isActive = true


        //MARK: Edit Profile Button
        self.editProfileButton.topAnchor.constraint(equalTo: nameContainer.bottomAnchor, constant: 0).isActive = true
        self.editProfileButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        self.editProfileButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20).isActive = true
        self.editProfileButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive = true

        //MARK: Highlights Container
        self.highlightsContainer.topAnchor.constraint(equalTo: editProfileButton.bottomAnchor, constant: 2).isActive = true
        self.highlightsContainer.heightAnchor.constraint(equalToConstant: 80).isActive = true
        self.highlightsContainer.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 10).isActive = true
        self.highlightsContainer.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10).isActive = true

        //MARK: Posts Collection View Container
        self.postsContainer.topAnchor.constraint(equalTo: highlightsContainer.bottomAnchor, constant: 8).isActive = true
        self.postsContainer.heightAnchor.constraint(equalToConstant: 700).isActive = true
        self.postsContainer.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        self.postsContainer.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
    }

}
