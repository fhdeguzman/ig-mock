//
//  IG_MockTests.swift
//  IG MockTests
//
//  Created by Franz Henri de Guzman on 10/19/20.
//  Copyright © 2020 Franz Henri de Guzman. All rights reserved.
//

import XCTest
@testable import IG_Mock

class IG_MockTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
